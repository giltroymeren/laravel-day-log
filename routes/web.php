<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/*
 * Day Log paths
 */
Route::get('/daylogs', 'DayLogController@showDashboard');

Route::get('/daylogs/create', 'DayLogController@get');

Route::post('/daylogs/create', 'DayLogController@create');

Route::get('/daylogs/view/{id}', 'DayLogController@get');

Route::get('/daylogs/update/{id}', 'DayLogController@get');

Route::post('/daylogs/update', 'DayLogController@update');

Route::delete('/daylogs/delete/{daylog}', 'DayLogController@delete');
