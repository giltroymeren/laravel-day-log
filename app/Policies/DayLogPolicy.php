<?php

namespace App\Policies;

use App\User;
use App\DayLog;
use Illuminate\Auth\Access\HandlesAuthorization;

class DayLogPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can delete the given daylog.
     *
     * @param  User  $user
     * @param  DayLog  $daylog
     * @return bool
     */
    public function delete(User $user, DayLog $daylog)
    {
        return $user->id === $daylog->user_id;
    }
}
