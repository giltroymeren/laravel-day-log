<?php

namespace App;

use App\User;
use App\Task;
use Illuminate\Database\Eloquent\Model;

class DayLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'location', 'log_at', 'category'];

   /**
     * Get the user that owns the day log.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the tasks for the user.
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
