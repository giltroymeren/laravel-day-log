<?php

namespace App\Http\Controllers;

use App\DayLog;
use App\Task;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DayLogRepository;

class DayLogController extends Controller
{
    /**
     * The day log repository instance.
     *
     * @var DayLogRepository
     */
    protected $daylogs;

    /**
     * Create a new controller instance.
     *
     * @param  DayLogRepository  $daylogs
     * @return void
     */
    public function __construct(DayLogRepository $daylogs)
    {
        $this->middleware('auth');

        $this->daylogs = $daylogs;
    }

    /**
     * Display Day Log dashboard.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showDashboard(Request $request)
    {
        return view('daylogs.index', [
            'daylogs' => $this->daylogs->getAllByUser($request->user()),
        ]);
    }

    /**
     * Show single day log form.
     *
     * @param  Request  $request
     * @param  Integer  $id
     * @return Response
     */
    public function get(Request $request, $id = null)
    {
        $form_action = $action = $daylog = null;

        if (!$request->is('daylogs/*'))
        {
            // TODO: Add proper 404 page
            return view('404');
        }

        $action = ucfirst(explode("/", $request->path())[1]);
        $form_action = strtolower($action);

        if ($action != 'Create')
        {
            $daylog = $this->daylogs->getByUser($request->user(), $id);
        }

        return view('daylogs.form', [
            'daylog' => $daylog,
            'action' => $action,
            'form_action' => '/daylogs/'.$form_action,
        ]);
    }

    /**
     * Create a new day log.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $taskCount = count($request->input('task_name.*'));
        // TODO: check if possible to get in view with Blade
        $request->session()->flash('taskCount', $taskCount);

        // TODO: Make DayLog and Task both model-binded once inside controller
        // wherein ie daylog.name and task.name.* are accessible in validation and CRUD
        $this->validateDayLog($request);

        $daylog = $request->user()->daylogs()->create([
            'name' => $request->daylog_name,
            'location' => $request->daylog_location,
            'log_at' => $request->daylog_log_at,
            'category' => $request->daylog_category,
        ]);

        for ($i = 1; $i <= $taskCount; $i++)
        {
            $daylog->tasks()->create([
                'name' => $request->input('task_name.'.$i),
                'start_at' => $request->input('task_start_at.'.$i),
                'end_at' => $request->input('task_end_at.'.$i),
            ]);
        }

        return redirect('/daylogs');
    }

    /**
     * Update a given day log.
     *
     * @param Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        $taskCount = count($request->input('task_name.*'));
        // TODO: check if possible to get in view with Blade
        $request->session()->flash('taskCount', $taskCount);

        $this->validateDayLog($request, $request->id);

        $daylog = $this->daylogs->getByUser($request->user(), $request->id);

        $daylog->update([
            'name' => $request->daylog_name,
            'location' => $request->daylog_location,
            'log_at' => $request->daylog_log_at,
            'category' => $request->daylog_category,
        ]);

        /*
         * TODO: Improve this update process
         *  currently delete and create Tasks per DayLog
         *  should be updateCreateOrDelete()
         */
        $daylog->tasks()->delete();
        // TODO: Find way to unify manual Task attribute form binding
        $tasks = [];
        foreach ($request->input('task_name.*') as $key => $value)
        {
            $tasks[$key]["name"] = $value;

        }
        foreach ($request->input('task_start_at.*') as $key => $value)
        {
            $tasks[$key]["start_at"] = $value;

        }
        foreach ($request->input('task_end_at.*') as $key => $value)
        {
            $tasks[$key]["end_at"] = $value;

        }

        foreach ($tasks as $task)
        {
            $daylog->tasks()->create([
                'name' => $task["name"],
                'start_at' => $task["start_at"],
                'end_at' => $task["end_at"],
            ]);
        }

        return redirect('/daylogs');
    }

    public function validateDayLog(Request $request, $dayLogID = null)
    {
        $rules = [
            'daylog_name' => 'required|max:1024',
            'daylog_location' => 'required|max:255',
            'daylog_log_at' => 'required|date_format:Y-m-d|unique:day_logs,log_at'
                . ($dayLogID ? ','.$dayLogID : ''),
            'daylog_category' => 'required|in:ADEQUATE,MINOR,MAJOR',
        ];

        if(count($request->input('task_name.*')) > 0 ||
            count($request->input('task_start_at.*')) > 0 ||
            count($request->input('task_end_at.*')) > 0)
        {
            $rules['task_name.*'] = 'required|max:255';
            $rules['task_start_at.*'] = 'required|date_format:H:i|before:task_end_at.*';
            $rules['task_end_at.*'] = 'required|date_format:H:i|after:task_start_at.*';
        }

        $this->validate($request, $rules);
    }

    /**
     * Delete the given day log.
     *
     * @param  Request  $request
     * @param  DayLog  $daylog
     * @return Response
     */
    public function delete(Request $request, DayLog $daylog = null)
    {
        $this->authorize('delete', $daylog);

        $daylog->tasks()->delete();
        $daylog->delete();

        return redirect('/daylogs');
    }
}
