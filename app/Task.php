<?php

namespace App;

use App\DayLog;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'start_at', 'end_at'];

   /**
     * Get the day log that owns the task.
     */
    public function dayLog()
    {
        return $this->belongsTo(DayLog::class);
    }

}
