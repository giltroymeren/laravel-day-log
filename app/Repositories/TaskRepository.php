<?php

namespace App\Repositories;

use App\Task;
use App\DayLog;

class TaskRepository
{
    /**
     * Get all of the tasks for a given day log.
     *
     * @param  DayLog  $daylog
     * @return Collection
     */
    public function getAllByDayLog(DayLog $daylog)
    {
        return $daylog->tasks()
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Get all of the tasks for a given day log.
     *
     * @param  DayLog  $daylog
     * @return Collection
     */
    public function getByDayLog(DayLog $daylog)
    {
        return $daylog->tasks()
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Get all of the tasks.
     *
     * @return Collection
     */
    public function getAll()
    {
        return Task::all();
    }
}