<?php

namespace App\Repositories;

use App\User;
use App\Task;
use App\Repositories\TaskRepository;

class DayLogRepository
{
    /**
     * The task repository instance.
     *
     * @var TaskRepository
     */
    protected $tasks;

    /**
     * Create a new controller instance.
     *
     * @param  TaskRepository  $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * Get all of the day logs for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function getAllByUser(User $user)
    {
        $daylogs = $user->daylogs()
            ->orderBy('created_at', 'asc')
            ->get();

        $tasks = $this->tasks->getAll();
        foreach($daylogs as $daylog)
        {
            $taskCount = 0;
            foreach($tasks as $task)
            {
                ($task->day_log_id === $daylog->id) ? $taskCount++ : '';
            }
            $daylog->taskCount = $taskCount;
        }

        return $daylogs;
    }

    /**
     * Get a day log for a given day log.
     *
     * @param  User  $user
     * @param  Integer  $id
     * @return Collection
     */
    public function getByUser(User $user, $id)
    {
        $daylog = $user->daylogs()
            ->where('id', $id)
            ->get()[0];

        $allTasks = $this->tasks->getAll();
        $tasks = [];

        foreach($allTasks as $task)
        {
            if ($task->day_log_id === $daylog->id)
            {
                $tasks[] = $task;
            }
        }

        return $daylog;
    }
}