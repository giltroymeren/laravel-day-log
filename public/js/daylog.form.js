$(document).ready(function() {
    $(document).on("focus", ".datepicker", function(){
        $(this).datetimepicker({ format: 'YYYY-MM-DD', });
    });
    $(document).on("focus", ".timepicker", function(){
        $(this).datetimepicker({ format: 'HH:mm', });
    });
});

function toggleTasksSection(element) {
    var inputs = document.querySelectorAll("#container-task-body input");

    if(document.getElementById("container-task-body").classList.contains('hidden')) {
        toggleTaskContainerDisplayState(false);
        toggleInputDisabled(inputs, false);

        element.classList.remove('btn-primary');
        element.classList.add('btn-danger');
        element.innerHTML = 'Delete Tasks';
    } else {
        toggleTaskContainerDisplayState(true);
        toggleInputDisabled(inputs, true);

        element.classList.remove('btn-danger');
        element.classList.add('btn-primary');
        element.innerHTML = 'Add Tasks';
    }

    return false;
}

function toggleTaskContainerDisplayState(toHide) {
    if(toHide) {
        document.getElementById("container-task-body").classList.add('hidden');
        document.getElementById("container-task-header").classList.add('hidden');
    } else {
        document.getElementById("container-task-body").classList.remove('hidden');
        document.getElementById("container-task-header").classList.remove('hidden');
    }
}

function toggleInputDisabled(array, state) {
    for(var i = 0; i < array.length; i++) {
        array[i].disabled = state;
    }
}

function addTaskRow() {
    var table = document.getElementById('tasks');
    var rowIndex = table.tBodies[0].rows.length + 1;
    var row = table.insertRow();

    var nameCell = row.insertCell(0);
    nameCell.append(getTaskRowCell('name', rowIndex));

    var startAtCell = row.insertCell(1);
    startAtCell.append(getTaskRowCell('start_at', rowIndex));

    var endAtCell = row.insertCell(2);
    endAtCell.append(getTaskRowCell('end_at', rowIndex));

    var deleteBtnCell = row.insertCell(3);
    var deleteBtn = document.getElementById('task_delete[1]').cloneNode(true);
    deleteBtn.setAttribute('id', 'task_delete[' + rowIndex + ']');
    deleteBtn.setAttribute('data-row-number', rowIndex);
    deleteBtnCell.append(deleteBtn);

    console.info('Added Task ' + rowIndex);
    return false;
}

function getTaskRowCell(name, rowIndex) {
    var formGroup = document.getElementById('container-task_' + name + '[1]').cloneNode();
    formGroup.setAttribute('id', 'container-task_' + name + '[' + rowIndex + ']');
    var input = document.getElementById('task_' + name + '[1]').cloneNode();
    input.setAttribute('name', 'task_' + name + '[' + rowIndex + ']');
    input.value = '';
    formGroup.appendChild(input);
    return formGroup;
}

function deleteTaskRow(element) {
    var table = document.getElementById('tasks');

    if(table.tBodies[0].rows.length === 1) {
        alert('Cannot have less than one task!');
    } else {
        table.deleteRow(element.getAttribute('data-row-number'));
    }
    return false;
}