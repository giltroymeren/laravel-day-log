<!-- resources/views/daylogs/form.blade.php -->

@extends('layouts.app')

@section('content')

    <?php $isView = ($action === 'View'); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $action }} Day Log
        </div>

        <form action="{{ $form_action }}" method="POST">
            {{ csrf_field() }}
            <div class="panel-body">

                @if (isset($daylog))
                <input type="hidden" name="id" id="update-daylog-id" class="form-control"
                    value="{{ $daylog->id }}">
                @endif

                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="daylog-name" class="col-sm-2 control-label">Day Log</label>

                        <div class="col-sm-10">
                            <textarea name="daylog_name" id="daylog-name" rows="3"
                                placeholder="Describe the day log"
                                {{ ($isView) ? 'disabled' : '' }}
                                class="form-control">{{ (isset($daylog)) ?
                                    $daylog->name : old('daylog_name') }}</textarea>
                            @if ($errors->has('daylog_name'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('daylog_name') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="daylog-location" class="col-sm-2 control-label">Location</label>

                        <div class="col-sm-10">
                            <input type="text" name="daylog_location"
                                id="daylog-location" class="form-control"
                                placeholder="Indicate the location of the log"
                                {{ ($isView) ? 'disabled' : '' }}
                                value="{{ (isset($daylog)) ?
                                    $daylog->location : old('daylog_location') }}">
                            @if ($errors->has('daylog_location'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('daylog_location') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="daylog-log_at"
                            class="col-sm-2 control-label">Date of Log</label>

                        <div class="col-sm-10">
                            <div class="input-group date">
                                <input type="text" class="form-control datepicker"
                                    name="daylog_log_at" id="daylog-log_at"
                                    placeholder="YYYY-MM-DD"
                                    {{ ($isView) ? 'disabled' : '' }}
                                    value="{{ (isset($daylog)) ?
                                        $daylog->log_at : old('daylog_log_at') }}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @if ($errors->has('daylog_log_at'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('daylog_log_at') }}
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="daylog-category" class="col-sm-2 control-label">Category</label>

                        <div class="col-sm-10">
                            <select class="form-control" name="daylog_category"
                                {{ ($isView) ? 'disabled' : '' }}>
                                <option value="" hidden>Select a category</option>
                                <option value="ADEQUATE"
                                    {{ ((isset($daylog)) ?
                                        ($daylog->category === "ADEQUATE" ? "selected" : "" ) :
                                            (old('daylog_category') === "ADEQUATE") ?
                                                "selected" : "") }}>
                                    ADEQUATE
                                </option>
                                <option value="MINOR"
                                    {{ ((isset($daylog)) ?
                                        ($daylog->category === "MINOR" ? "selected" : "" ) :
                                            (old('daylog_category') === "MINOR") ?
                                                "selected" : "") }}>
                                    MINOR
                                </option>
                                <option value="MAJOR"
                                    {{ ((isset($daylog)) ?
                                        ($daylog->category === "MAJOR" ? "selected" : "" ) :
                                            (old('daylog_category') === "MAJOR") ?
                                                "selected" : "") }}>
                                    MAJOR
                                </option>
                            </select>
                            @if ($errors->has('daylog_category'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('daylog_category') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <?php $hasTasksByPost = (Session::has('taskCount') && Session::get('taskCount') > 0);
                $hasTasksByGet = (isset($daylog->tasks) && count($daylog->tasks) > 0);
                $hasTasksByDefault = ($hasTasksByPost || ($hasTasksByGet && !$isView)); ?>

            <ul class="list-group">
                <li class="list-group-item text-center {{ ($isView) ? 'hidden' : '' }}">
                    <button class="btn {{ ($hasTasksByDefault) ? 'btn-danger' : 'btn-primary' }}"
                        onclick="return toggleTasksSection(this);">
                        {{ ($hasTasksByDefault) ? 'Delete' : 'Add' }} Tasks
                    </button>
                </li>
                <li class="list-group-item {{ (($hasTasksByGet) || $hasTasksByPost) ?
                    '' : 'hidden' }}" id="container-task-header">
                    Tasks
                </li>
                <li class="list-group-item clearfix {{ (($hasTasksByGet) || $hasTasksByPost) ?
                    '' : 'hidden' }}" id="container-task-body">
                    <table class="table" id="tasks">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th width="15%">Start Time</th>
                                <th width="15%">End Time</th>
                                <th width="2.5%" class="{{ ($isView) ? 'hidden' : '' }}"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if ($hasTasksByPost)
                            @for ($i = 1; $i <= Session::get('taskCount'); $i++)
                                @include('daylogs.tasks', ['key' => ($i)])
                            @endfor
                        @elseif ($hasTasksByGet)
                            @foreach ($daylog->tasks as $task)
                                @include('daylogs.tasks', ['key' => ($loop->index + 1)])
                            @endforeach
                        @else
                            <!-- Default number of Tasks -->
                            @include('daylogs.tasks', ['key' => 1])
                        @endif
                        </tbody>
                    </table>
                    <div class="form-group clearfix {{ ($isView) ? 'hidden' : '' }}">
                        <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-primary"
                                onclick="return addTaskRow();">
                                Add Task
                            </button>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="panel-footer clearfix">
                <div class="form-group">
                    <div class="col-sm-6">
                        <a href="/daylogs" role="button" class="btn btn-danger">
                            Back
                        </a>
                    </div>

                    <div class="col-sm-6 text-right {{ ($isView) ? 'hidden' : '' }}">
                        <button type="reset" class="btn btn-danger">
                            Reset
                        </button>
                        <button type="submit" class="btn btn-primary">
                            {{ $action }}
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </div>

    <script type="text/javascript" src="{{ URL::asset('js/daylog.form.js') }}"></script>

@endsection