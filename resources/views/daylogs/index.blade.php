<!-- resources/views/daylogs/index.blade.php -->

@extends('layouts.app')

@section('content')

    @if (count($daylogs) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                All Day Logs
            </div>

            <table class="table table-hover daylog-table">

                <thead>
                    <th class="text-right">Date</th>
                    <th width="50%">Day Log</th>
                    <th class="text-right"># of Tasks</th>
                    <th class="text-right">Created</th>
                    <th>Update</th>
                    <th>Delete</th>
                </thead>

                <tbody>
                    @foreach ($daylogs as $daylog)
                        <tr class="{{ ($daylog->created_at != $daylog->updated_at) ?
                            'warning' : '' }}">
                            <td class="table-text text-right">
                                <div><code>{{ $daylog->log_at }}</code></div>
                            </td>

                            <td class="table-text">
                                <div>
                                    <a href="/daylogs/view/{{ $daylog->id }}">
                                        {{ $daylog->name }}
                                    </a>
                                </div>
                            </td>

                            <td class="table-text text-right">
                                <div>{{ $daylog->taskCount }}</div>
                            </td>

                            <td class="table-text text-right">
                                <div><code>{{ $daylog->created_at->format('Y-m-d') }}</code></div>
                            </td>

                            <td>
                                <form action="/daylogs/update/{{ $daylog->id }}" method="GET">
                                    {{ csrf_field() }}
                                    {{ method_field('UPDATE') }}

                                    <button class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-pencil"
                                            aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="/daylogs/delete/{{ $daylog->id }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button class="btn btn-danger btn-xs"
                                        onclick="return confirmDayLogAction('Delete');">
                                        <span class="glyphicon glyphicon-remove"
                                            aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
            function confirmDayLogAction(action) {
                return (confirm(action + ' this Day Log?')) ? true : false;
            }
        </script>
    @endif

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/daylogs/create" method="GET"
                class="text-right">
                {{ csrf_field() }}

                <button class="btn btn-primary">
                    <span class="glyphicon glyphicon-plus"
                        aria-hidden="true"></span> Create
                </button>
            </form>
        </div>
    </div>

@endsection