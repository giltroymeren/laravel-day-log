<!-- resources/views/daylogs/tasks.blade.php -->

<tr>
    <td>
        <div class="form-group" id="container-task_name[{{ $key }}]">
            <input type="text"
                class="form-control"
                name="task_name[{{ $key }}]"
                id="task_name[{{ $key }}]"
                placeholder="Name of task"
                {{ ($hasTasksByDefault) ? '' : 'disabled' }}
                value="{{ (isset($task)) ? $task->name : old('task_name.'.$key) }}" />
            @if ($errors->has('task_name.'.$key))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('task_name.'.$key) }}
                </div>
            @endif
        </div>
    </td>
    <td>
        <div class="form-group" id="container-task_start_at[{{ $key }}]">
            <input type="text"
                class="form-control timepicker"
                name="task_start_at[{{$key}}]"
                id="task_start_at[{{$key}}]"
                placeholder="HH:MM"
                {{ ($hasTasksByDefault) ? '' : 'disabled' }}
                value="{{ date('H:i', strtotime((isset($task)) ?
                    $task->start_at : old('task_start_at.'.$key))) }}" />
            @if ($errors->has('task_start_at.'.$key))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('task_start_at.'.$key) }}
                </div>
            @endif
        </div>
    </td>
    <td>
        <div class="form-group" id="container-task_end_at[{{ $key }}]">
            <input type="text"
                class="form-control timepicker"
                name="task_end_at[{{$key}}]"
                id="task_end_at[{{$key}}]"
                placeholder="HH:MM"
                {{ ($hasTasksByDefault) ? '' : 'disabled' }}
                value="{{ date('H:i', strtotime((isset($task)) ?
                    $task->end_at : old('task_end_at.'.$key))) }}" />
            @if ($errors->has('task_end_at.'.$key))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('task_end_at.'.$key) }}
                </div>
            @endif
        </div>
    </td>
    <td class="{{ ($isView) ? 'hidden' : '' }}">
        <button class="btn btn-danger" id="task_delete[{{ $key }}]"
            data-row-number="{{ $key }}"
            onclick="return deleteTaskRow(this);">
            <span class="glyphicon glyphicon-remove"
                aria-hidden="true"></span>
        </button>
    </td>
</tr>
